import React, { Component } from 'react';

import { withTracker } from 'meteor/react-meteor-data'
import io from 'socket.io-client'
import { getDisplayStream } from '../helpers/media-access';
import ShareScreenIcon from './ShareScreenIcon';
import Recorder from '../helpers/recorder';
import Chats from '../api/chats';
//import { ChatRecords } from '../api/chatList.js';


import MidiaCall from '../helpers/simple-peer'
import DisconnectIcon from './disconnectIcon';
const socket = io('http://localhost:8080')
class Video extends Component {
  constructor() {
    super()
    this.state = {
      localStream: {},
      remoteStreamUrl: '',
      streamUrl: '',
      initiator: false,
      peer: {},
      full: false,
      connecting: false,
      waiting: true,
      recordbtn: 'Record'
    }
  }

  videoCall = new MidiaCall();
  recorder = new Recorder();

  componentDidMount() {
    
    const component = this
    this.setState({ socket })
    const { roomId } = this.props.match.params

    this.recorder.init();

    this.getUserMedia().then(() => {
      socket.emit('join', { roomId: roomId })
    })
    socket.on('init', () => {
      this.setState({ initiator: true })
    })
    socket.on('ready', () => {
      this.enter(roomId)
    })
    socket.on('desc', data => {
      if (data.type === 'offer' && component.state.initiator) return
      if (data.type === 'answer' && !component.state.initiator) return
      this.call(data)
    })
    socket.on('disconnected', () => {
      this.setState({ initiator: true })
      this.setState({ connecting: false, waiting: false })
    })
    socket.on('full', () => {
      component.setState({ full: true })
    })
  }
  getUserMedia(cb) {
    return new Promise((resolve, reject) => {
      navigator.getUserMedia = navigator.getUserMedia =
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia
      const op = {
        video: {
          width: { min: 160, ideal: 640, max: 1280 },
          height: { min: 120, ideal: 360, max: 720 }
        },
        audio: true
      }
      navigator.getUserMedia(
        op,
        stream => {
          this.setState({ streamUrl: stream, localStream: stream })
          this.localVideo.srcObject = stream
          resolve()
        },
        () => {}
      )
    })
  }
  getDisplay(){
    getDisplayStream().then(stream => {
      stream.oninactive = () => {
        this.state.peer.removeStream(this.state.localStream)  
        this.getUserMedia().then(() => {
          this.state.peer.addStream(this.state.localStream)  
        })
      }
      this.setState({ streamUrl: stream, localStream: stream })
      this.localVideo.srcObject = stream   
      this.state.peer.addStream(stream)   
    })
  }

  disconnectCall(){
    this.setState({ connecting: false, waiting: false });
    socket.emit('disconnected');
    this.localVideo.srcObject.getTracks().forEach((track) => {
      track.stop();
    });
    this.props.history.push('/')
  }

  enter = roomId => {
    this.setState({ connecting: true })
    const peer = this.videoCall.init(
      this.state.localStream,
      this.state.initiator
    )
    this.setState({peer})
    
    peer.on('signal', data => {
      const signal = {
        room: roomId,
        desc: data
      }
      this.state.socket.emit('signal', signal);
      
    })
    peer.on('stream', stream => {
      this.remoteVideo.srcObject = stream
      this.setState({ connecting: false, waiting: false })
      this.recorder.handleSuccess(stream);
    })
    peer.on('disconnected', () => {
      this.setState({ connecting: false, waiting: false })
    })

    peer.on('error', function(err) {
      console.log(err)
    })
  }
  call = otherId => {
    this.videoCall.connect(otherId)
  }
  renderFull = () => {
    if (this.state.full) {
      return 'The room is full'
    }
  }

  Record=()=>{
    if(this.state.recordbtn === "Record"){
      this.recorder.startRecording();
      this.setState({ recordbtn: "Stop" })
    }
    else{
      this.recorder.stopRecording();
      this.recorder.saveRecordig();
      this.setState({ recordbtn: "Record" });
      
    }
    
  }

  playRecord=(task)=>{
    console.log("Recorder video",task);
    var blob = new Blob([task.fileInfo.file],{type: task.fileInfo.type});
    var src = URL.createObjectURL(blob);
	 console.log("Recorder video url",src);
  }




  getChatsRecords=()=>{
    console.log(this.props.chats, "<====");
    
    return this.props.chats.map((task) => (
      <li key={task._id}>
      <button className="button" onClick={this.playRecord(task)}>
        play
      </button>
      <span className="text">{task._id}</span>
    </li>
     
    ));
  }

  
  
  render() {
    return (
      <div className="video-wrapper">
        <div className="local-video-wrapper">
          <video
            autoPlay
            id="localVideo"
            muted
            ref={video => (this.localVideo = video)}
          />
        </div>
        <video
          autoPlay
          className={`${
            this.state.connecting || this.state.waiting ? 'hide' : ''
          }`}
          id="remoteVideo"
          ref={video => (this.remoteVideo = video)}
        />
        <div className="recordingList">
        <ul>{this.getChatsRecords()}</ul>
        </div>
        <button className="share-screen-btn" onClick={() => {
          this.getDisplay()
        }}><ShareScreenIcon/></button>


        <button 
        
        className={`${
          this.state.connecting || this.state.waiting ? 'hide share-screen-btn' : 'share-screen-btn'
        }`}

        onClick={() => {
          this.Record()
        }}>{this.state.recordbtn}</button>


        <button className="close-screen-btn" onClick={() => {
          this.disconnectCall()
        }}><DisconnectIcon/></button>
        {this.state.connecting && (
          <div className="status">
            <p>Establishing connection...</p>
          </div>
        )}
        {this.state.waiting && (
          <div className="status">
            <p>Waiting for someone...</p>
          </div>
        )}
        {this.renderFull()}
      </div>
    )
  }
}

export default withTracker(() => {
  return {
    chats: Chats.find({}, { sort: { createdAt: -1 } }).fetch(),
  };
})(Video); 
