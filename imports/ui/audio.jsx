import React, { Component } from 'react';
import MidiaCall from '../helpers/simple-peer'
import io from 'socket.io-client'
import DisconnectIcon from './disconnectIcon';
import MicRecorder from 'mic-recorder-to-mp3';
const Mp3Recorder = new MicRecorder({ bitRate: 128 });

const socket = io('http://localhost:8080')
class Audio extends Component {
  constructor() {
    super()
    this.state = {
      localStream: {},
      remoteStreamUrl: '',
      streamUrl: '',
      initiator: false,
      peer: {},
      full: false,
      connecting: false,
      waiting: true,
      isRecording: false,
      blobURL: '',
      isBlocked: false,
    }
  }
  audioCall = new MidiaCall()
  componentDidMount() {
    const component = this
    this.setState({ socket })
    const { roomId } = this.props.match.params
    this.getUserMedia().then(() => {
      socket.emit('join', { roomId: roomId })
    })
    socket.on('init', () => {
      this.setState({ initiator: true })
    })
    socket.on('ready', () => {
      this.enter(roomId)
    })
    socket.on('desc', data => {
      if (data.type === 'offer' && component.state.initiator) return 
      if (data.type === 'answer' && !component.state.initiator) return
      this.call(data)
    })
    socket.on('disconnected', () => {
      this.setState({ initiator: true })
    })
    socket.on('full', () => {
      component.setState({ full: true })
    })
  }
  /**
   * to start audio record
   */
  start = () => {
    if (this.state.isBlocked) {
      console.log('Permission Denied');
    } else {
      Mp3Recorder
        .start()
        .then(() => {
          this.setState({ isRecording: true });
        }).catch((e) => console.error(e));
    }
  };

  /**
   * to stop audio record
   */
  stop = () => {
    Mp3Recorder
      .stop()
      .getMp3()
      .then(([buffer, blob]) => {
        const blobURL = URL.createObjectURL(blob)
        this.setState({ blobURL, isRecording: false });
      }).catch((e) => console.log(e));
  };

  getUserMedia(cb) {
    return new Promise((resolve, reject) => {
      navigator.getUserMedia = navigator.getUserMedia =
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia
      const op = {
        video: false,
        audio: true
      }
      navigator.getUserMedia(
        op,
        stream => {
          this.setState({ streamUrl: stream, localStream: stream })
          this.localAudio.srcObject = stream
          resolve()
        },
        () => {}
      )
    })
  }
  enter = roomId => {
    this.setState({ connecting: true })
    const peer = this.audioCall.init(
      this.state.localStream,
      this.state.initiator
    )
    this.setState({peer})
    
    peer.on('signal', data => {
      const signal = {
        room: roomId,
        desc: data
      }
      this.state.socket.emit('signal', signal)
    })
    peer.on('stream', stream => {
      this.remoteAudio.srcObject = stream
      this.setState({ connecting: false, waiting: false })
    })
    peer.on('error', function(err) {
      console.log(err)
    })
  }
  call = otherId => {
    this.audioCall.connect(otherId)
  }
  renderFull = () => {
    if (this.state.full) {
      return 'The room is full'
    }
  }

  disconnectAudioCall(){
    this.setState({ connecting: false, waiting: false });
    socket.emit('disconnected');
    this.props.history.push('/');
    this.localAudio.srcObject.getTracks().forEach((track) => {
        track.stop();
    });
  }

  render() {
    return (
      <div className="video-wrapper">
           <div>
            <button className="record-btn" onClick={this.start} disabled={this.state.isRecording}>
            Record
            </button>
            <button className="stop-btn" onClick={this.stop} disabled={!this.state.isRecording}>
            Stop
            </button>
            <audio src={this.state.blobURL} controls="controls" />
        </div>
        <div className="local-video-wrapper">
          <audio
            autoPlay
            id="localAudio"
            muted
            ref={audio => (this.localAudio = audio)}
          />
        </div>
        <audio
          autoPlay
          className={`${
            this.state.connecting || this.state.waiting ? 'hide' : ''
          }`}
          id="remoteAudio"
          ref={audio => (this.remoteAudio = audio)}
        />
        <button className="close-screen-btn"onClick={() => {
          this.disconnectAudioCall()
        }}>
          <DisconnectIcon/>
        </button>
        {this.state.connecting && (
          <div className="status">
            <p>Establishing connection...</p>
          </div>
        )}
        {this.state.waiting && (
          <div className="status">
            <p>Waiting for someone...</p>
          </div>
        )}
        {this.renderFull()}

       
      </div>
    )
  }
}

export default Audio
