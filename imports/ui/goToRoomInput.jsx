import React, { useState } from 'react'
import shortId from 'shortid'

const goToRoom = (history, roomId) => {
  history.push(`/${roomId}`);
}

const goToAudioRoom = (history, roomId, isAudio) => {
  history.push(`/audio/${roomId}`);
}

const goToAudioRecord = (history, roomId, isAudio) => {
  history.push(`/record`);
}


export function goToRoomInput({history}) {
  let [roomId, setRoomId] = useState(shortId.generate());

  return (<div className="enter-room-container">
    <form>
          <input type="text" value={roomId} placeholder="Room id" onChange={(event) => {
            setRoomId(event.target.value)
          }}/>
          <button className="button-pointer" type="button" onClick={() => {
            goToRoom(history, roomId)
          }}>Connect to video call</button>
          <button className="button-pointer" type="button" onClick={() => {
            goToAudioRoom(history, roomId, true)
          }}>Connect to audio</button>
    </form>
  </div>)
}