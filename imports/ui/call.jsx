import React, { Component } from 'react';
import SimplePeer from 'simple-peer';
import wrtc from 'wrtc';

export default class Call extends Component {

    videoPlayer = '';
    peer1 = '';
    peer2 = '';
    state = {
      source: ""
    }
    onSuccessWithSrcObject(stream) {
      this.videoPlayer.srcObject = stream;
   }
   
   onSuccessWithSrc(stream) {
      this.videoPlayer.src = window.URL.createObjectURL(stream);
   }
    componentDidMount(){

      this.peer1 = new SimplePeer({ initiator: true, wrtc: wrtc });
      this.peer2 = new SimplePeer({ wrtc: wrtc });
      
      this.peer1.on('signal', data => {
        this.peer2.signal(data)
      })
       
      this.peer2.on('signal', data => {
        this.peer1.signal(data)
      })
             
    }

    addMedia (stream) {
      this.peer1.addStream(stream) // <- add streams to peer dynamically
    }
  
    videoCall(){

      navigator.mediaDevices.getUserMedia({ audio: true, video: true })
      .then(stream => {
        this.videoPlayer = document.getElementById("video-player");
        this.videoPlayer = document.getElementById("audio-player")
       
        if ('srcObject' in this.videoPlayer) {
          this.videoPlayer.srcObject = stream
        } else {
          this.videoPlayer.src = window.URL.createObjectURL(stream) // for older browsers
        }
       
        this.videoPlayer.play()


      })
      .catch( err => alert(`Bummer! ${err}.`))
    }
  
    render() {
      return (
        <div>
          <video id="video-player" src={this.state.source} autoPlay="true" autoPlay>
            <audio id="audio-player" src={this.state.source} autoPlay="true" autoPlay></audio>
          </video>
          <button className="button-cls" onClick={() => this.videoCall()}>Video</button>
        </div>
      )
    }
}
