import React, { Component } from 'react';
import Video from './video'
import Audio from './audio'

import { BrowserRouter, Route } from 'react-router-dom';
import { goToRoomInput } from './goToRoomInput';

const App = () => (
  <div>
    <BrowserRouter>
       <React.Fragment>
          <Route path="/" exact component={goToRoomInput}/>
          <Route path="/:roomId" exact component={Video}/>
          <Route path="/audio/:roomId" exact component={Audio}/>
        </React.Fragment>
      </BrowserRouter>
  </div>
);

export default App;
