import { withTracker } from 'meteor/react-meteor-data';
import Chats from '../api/chats';
export default class Recorder{
     mediaSource = null;
      mediaRecorder= null;
    recordedBlobs= null;
     sourceBuffer= null;

     init=()=>{
         this.mediaSource= new MediaSource();
         this.mediaSource.addEventListener('sourceopen',this.handleSourceOpen, false);
     }

     startRecording=()=> {
        this.recordedBlobs = [];
        let options = {mimeType: 'video/webm;codecs=vp9'};
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
          console.error(`${options.mimeType} is not Supported`);
        }
      
        try {
          this.mediaRecorder = new MediaRecorder(window.stream, options);
        } catch (e) {
          console.error('Exception while creating MediaRecorder:', e);
          return;
        }
      
        console.log('Created MediaRecorder', this.mediaRecorder, 'with options', options);
        this.mediaRecorder.onstop = (event) => {
          console.log('Recorder stopped: ', event);
          console.log('Recorded Blobs: ', this.recordedBlobs);
        };
        this.mediaRecorder.ondataavailable = this.handleDataAvailable;
        this.mediaRecorder.start(10); // collect 10ms of data
      }
      
       stopRecording=()=> {
        this.mediaRecorder.stop();
      }
      
       handleSuccess=(stream)=> {
        window.stream = stream;
      }


     handleSourceOpen=(event)=> {
        sourceBuffer = this.mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
      }
      
     handleDataAvailable=(event)=> {
        if (event.data && event.data.size > 0) {
            this.recordedBlobs.push(event.data);
        }
      }

      saveRecordig= () => {
        const blob = new Blob(this.recordedBlobs, {type: 'video/webm'});

        this.BinaryFileReader.read(blob, function(err, fileInfo){
            Chats.insert({fileInfo,createdAt: new Date() })
          });

      };

     BinaryFileReader = {
        read: function(file, callback){
          var reader = new FileReader;
      
          var fileInfo = {
            name: file.name,
            type: file.type,
            size: file.size,
            file: null
          }
      
          reader.onload = function(){
            fileInfo.file = new Uint8Array(reader.result);
            callback(null, fileInfo);
          }
          reader.onerror = function(){
            callback(reader.error);
          }
      
          reader.readAsArrayBuffer(file);
        }
      }

}