import Peer from 'simple-peer'

export default class MidiaCall {
    peer = null 
    init = (stream, initiator) => {
        this.peer = new Peer({
            initiator: initiator,
            stream: stream,
            trickle: false,
            reconnectTimer: 1000,
            iceTransportPolicy: 'relay',
            config: {
                iceServers: [
                    { urls: process.env.REACT_APP_STUN_SERVERS ? process.env.REACT_APP_STUN_SERVERS.split(',') : 'stun:stun4.l.google.com:19302'.split(',') },
                    {
                        urls: process.env.REACT_APP_TURN_SERVERS ? process.env.REACT_APP_TURN_SERVERS.split(',') : 'turn:numb.viagenie.ca'.split(','),
                        username: process.env.REACT_APP_TURN_USERNAME ? process.env.REACT_APP_TURN_USERNAME : '[email]',
                        credential: process.env.REACT_APP_TURN_CREDENCIAL ? process.env.REACT_APP_TURN_CREDENCIAL : "[password]",
                    },
                ]
            }
        })
        return this.peer
    }
    connect = (otherId) => {
        this.peer.signal(otherId)
    }  
} 